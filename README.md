# Sakura Engine

WIP personal library for writing games with C++ and SDL 2

Uses C++14 and attempts to adhere to modern C++ best practices

Supports Emscripten

Named after Nene Sakura, an aspiring C++ game programmer from the 4-koma and
anime New Game

Don't expect a stable API anytime soon
