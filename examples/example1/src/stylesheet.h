#ifndef EXAMPLE1_STYLESHEET_H
#define EXAMPLE1_STYLESHEET_H

#include <sakura/style/sheet.h>

namespace example1 {
    sakura::style::Sheet stylesheet();
}

#endif
