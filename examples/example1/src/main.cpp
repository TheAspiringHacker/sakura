#include "stylesheet.h"
#include "scene.h"
#include <memory>
#include <string>
#include <sakura/app.h>

int main()
{
    auto stylesheet = example1::stylesheet();
    std::unique_ptr<example1::Scene> title_scene =
        std::make_unique<example1::Scene>();
    return sakura::App::main(
        stylesheet,
        std::move(title_scene),
        "Game",
        480,
        360
    );
}
