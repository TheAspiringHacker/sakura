#include "sakura/sdl.h"
#include <string>

sakura::sdl::Mask sakura::sdl::mask()
{
    // Switch to constexpr if when I get access to C++17
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    return { 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff };
#else
    return { 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000 };
#endif
}

sakura::sdl::Initialize::Initialize()
{
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_EVENTS) != 0) {
        throw std::runtime_error(std::string(SDL_GetError()));
    }
    int img_flags = IMG_INIT_PNG;
    if((IMG_Init(img_flags) & img_flags) != img_flags) {
        throw std::runtime_error(std::string(IMG_GetError()));
    }
    if(TTF_Init() == -1) {
        throw std::runtime_error(std::string(TTF_GetError()));
    }
}

sakura::sdl::Initialize::~Initialize()
{
    // Run quit functions in reverse initialization order just to be safe
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void sakura::sdl::Destroy_Window::operator()(SDL_Window* ptr) const
{
    SDL_DestroyWindow(ptr);
}

void sakura::sdl::Destroy_Texture::operator()(SDL_Texture* ptr) const
{
    SDL_DestroyTexture(ptr);
}

void sakura::sdl::Free_Surface::operator()(SDL_Surface* ptr) const
{
    SDL_FreeSurface(ptr);
}

void sakura::sdl::Destroy_Renderer::operator()(SDL_Renderer* ptr) const
{
    SDL_DestroyRenderer(ptr);
}

void sakura::sdl::Close_Font::operator()(TTF_Font* ptr) const
{
    TTF_CloseFont(ptr);
}

sakura::sdl::Unique_Surface sakura::sdl::load_image(const std::string& filename)
{
    auto uptr = sakura::sdl::Unique_Surface(IMG_Load(filename.c_str()));
    if(uptr.get() == nullptr) {
        throw std::runtime_error(std::string(IMG_GetError()));
    }
    return uptr;
}
