#include "sakura/renderer.h"

sakura::Renderer::Renderer(const std::string& name, int w, int h, Uint32 flags)
{
    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;
    SDL_CreateWindowAndRenderer(w, h, flags, &window, &renderer);
    m_window = sakura::sdl::Unique_Window(window);
    m_renderer = sakura::sdl::Unique_Renderer(renderer);
    SDL_SetWindowTitle(m_window.get(), name.c_str());

    // Clear the window to black
    SDL_SetRenderDrawColor(m_renderer.get(), 0, 0, 0, 255);
    SDL_RenderClear(m_renderer.get());
}

SDL_Renderer& sakura::Renderer::get()
{
    return *m_renderer;
}

const SDL_Renderer& sakura::Renderer::get() const
{
    return *m_renderer;
}

sakura::sdl::Unique_Texture
sakura::Renderer::create_texture_from_surface(SDL_Surface& surface)
{
    auto texture = sakura::sdl::Unique_Texture(
        SDL_CreateTextureFromSurface(m_renderer.get(), &surface)
    );
    if(texture.get() == nullptr) {
        throw std::runtime_error(std::string(SDL_GetError()));
    }
    return texture;
}

void sakura::Renderer::draw_texture(SDL_Texture& texture, int x, int y)
{
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    SDL_QueryTexture(&texture, nullptr, nullptr, &rect.w, &rect.h);
    SDL_RenderCopy(m_renderer.get(), &texture, nullptr, &rect);
}

void sakura::Renderer::draw_subtexture(
    SDL_Texture& texture, int x, int y, const SDL_Rect& rect
)
{
    SDL_Rect dest_rect;
    dest_rect.x = x;
    dest_rect.y = y;
    dest_rect.w = rect.w;
    dest_rect.h = rect.h;
    SDL_RenderCopy(m_renderer.get(), &texture, &rect, &dest_rect);
}

sakura::sdl::Unique_Texture sakura::Renderer::draw_text(
    const std::string& str,
    TTF_Font& font,
    SDL_Color color)
{
    auto surface = sakura::sdl::Unique_Surface(TTF_RenderText_Solid(
        &font,
        str.c_str(),
        color
    ));
    return sakura::sdl::Unique_Texture(
        SDL_CreateTextureFromSurface(m_renderer.get(), surface.get())
    );
}

void sakura::Renderer::draw_text(
    const std::string& str,
    const sakura::Font& font,
    int x, int y)
{
    font.render(*this, str, x, y);
}

void sakura::Renderer::draw_line(int x1, int y1, int x2, int y2)
{
    SDL_RenderDrawLine(m_renderer.get(), x1, y1, x2, y2);
}

void sakura::Renderer::draw_line(const SDL_Point& p1, const SDL_Point& p2)
{
    draw_line(p1.x, p1.y, p2.x, p2.y);
}

void sakura::Renderer::draw_lines(const std::vector<SDL_Point>& points)
{
    SDL_RenderDrawLines(m_renderer.get(), points.data(), points.size());
}

void sakura::Renderer::draw_rect(SDL_Rect& rect)
{
    SDL_RenderDrawRect(m_renderer.get(), &rect);
}

void sakura::Renderer::draw_rects(const std::vector<SDL_Rect>& rects)
{
    SDL_RenderDrawRects(m_renderer.get(), rects.data(), rects.size());
}

void sakura::Renderer::preupdate()
{
    SDL_RenderClear(m_renderer.get());
}

void sakura::Renderer::update()
{
    SDL_RenderPresent(m_renderer.get());
}
