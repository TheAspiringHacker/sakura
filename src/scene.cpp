#include "sakura/scene.h"

const std::forward_list<sakura::Entity*>& sakura::Scene::entity_queue()
{
    return m_entities;
}

void sakura::Scene::clear_entity_queue()
{
    m_entities.clear();
}

void sakura::Scene::add_entity(sakura::Entity& entity)
{
    m_entities.push_front(&entity);
}
