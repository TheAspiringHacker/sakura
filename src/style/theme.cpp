#include "sakura/style/theme.h"

sakura::style::Theme::Theme(
    sakura::Renderer& renderer,
    const sakura::style::Sheet& stylesheet)
{
    for(auto& font : stylesheet.fonts) {
        auto font_ptr = sakura::sdl::Unique_Font(
            TTF_OpenFont(font.second.filename.c_str(), font.second.size)
        );
        if(font_ptr.get() == nullptr) {
            throw std::runtime_error(
                "Unable to open font: " + font.second.filename
            );
        }

        m_fonts.emplace(
            font.first, sakura::Font(renderer, *font_ptr, font.second.color)
        );
    }
}

const sakura::Font& sakura::style::Theme::font_at(const std::string& key) const
{
    return m_fonts.at(key);
}
