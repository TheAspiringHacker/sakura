#ifndef SAKURA_APP_H
#define SAKURA_APP_H

#include "entity.h"
#include "input.h"
#include "observer.h"
#include "renderer.h"
#include "scene.h"
#include "style/sheet.h"
#include <memory>
#include <string>
#include <SDL2/SDL.h>

namespace sakura {
    class App
    {
        sakura::Observer<Entity> m_entities;
        std::unique_ptr<Scene> m_scene;
        std::unique_ptr<Scene> m_next_scene;
        Input m_input;
        Renderer m_renderer;
        style::Theme m_theme;

        App(std::unique_ptr<Scene>, style::Sheet, Input, Renderer);

        void tick();

        static void emscr_main_loop(void*);
    public:
        void change_scene(std::unique_ptr<Scene>);

        style::Theme& theme();

        Input& input();

        Renderer& renderer();

        static int main(
            const style::Sheet& stylesheet,
            std::unique_ptr<Scene> scene,
            const std::string& window_name,
            int width,
            int height,
            Uint32 flags = 0);
    };
}

#endif
