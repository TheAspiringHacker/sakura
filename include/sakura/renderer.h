#ifndef SAKURA_RENDERER_H
#define SAKURA_RENDERER_H

#include "sdl.h"
#include "style/theme.h"

namespace sakura {
    class Font;
    class Renderer
    {
    public:
        Renderer(const std::string&, int, int, Uint32 = 0);
        Renderer(const Renderer&)=delete;
        Renderer(Renderer&&)=default;
        virtual ~Renderer()=default;

        SDL_Renderer& get();
        const SDL_Renderer& get() const;

        sdl::Unique_Texture create_texture_from_surface(SDL_Surface&);

        void draw_texture(SDL_Texture&, int, int);

        void draw_subtexture(SDL_Texture&, int, int, const SDL_Rect&);

        sdl::Unique_Texture
        draw_text(const std::string&, TTF_Font&, SDL_Color);

        void draw_text(const std::string&, const Font&, int, int);

        void draw_line(int, int, int, int);

        void draw_line(const SDL_Point&, const SDL_Point&);

        void draw_lines(const std::vector<SDL_Point>&);

        void draw_rect(SDL_Rect&);

        void draw_rects(const std::vector<SDL_Rect>&);

        void preupdate();

        void update();
    private:
        sdl::Unique_Window m_window;
        sdl::Unique_Renderer m_renderer;
    };
}

#endif
