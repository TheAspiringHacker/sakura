#ifndef SAKURA_ENTITY_H
#define SAKURA_ENTITY_H

#include "observer.h"
#include "sdl.h"

namespace sakura {
    class App;
    class Entity
    {
    public:
        virtual ~Entity()=default;

        void add_observer(Observer<Entity>&);

        virtual void input(const SDL_Event&);

        virtual void update(App&);
    private:
        Subject<Entity> m_subject;
    };
}

#endif
