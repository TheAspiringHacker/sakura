#ifndef SAKURA_STYLE_SHEET_H
#define SAKURA_STYLE_SHEET_H

#include "../sdl.h"
#include <string>
#include <unordered_map>
#include <utility>

namespace sakura {
    namespace style {
        struct Font
        {
            std::string filename;
            int size;
            SDL_Color color;
        };

        struct Sheet
        {
            Sheet()=default;

            // Takes map by value because it will be moved; see
            // https://stackoverflow.com/q/10231349/8887578
            Sheet(std::unordered_map<std::string, Font>);

            std::unordered_map<std::string, Font> fonts;
        };
    }
}

#endif
