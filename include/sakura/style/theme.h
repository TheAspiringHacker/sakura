#ifndef SAKURA_STYLE_THEME_H
#define SAKURA_STYLE_THEME_H

#include "sheet.h"
#include "../sdl.h"
#include "../font.h"
#include "../renderer.h"
#include <unordered_map>

namespace sakura {
    namespace style {
        class Theme
        {
        public:
            Theme(Renderer&, const Sheet&);
            Theme(const Theme&) = delete;
            Theme(Theme&&) = default;
            const sakura::Font& font_at(const std::string&) const;
        private:
            std::unordered_map<std::string, sakura::Font> m_fonts;
        };
    }
}

#endif
