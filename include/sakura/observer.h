#ifndef SAKURA_OBSERVER_H
#define SAKURA_OBSERVER_H

#include <forward_list>
#include <utility>

namespace sakura {
    /**
       These two template classes allow the observer to keep a forward list of
       pointers to subjects with shorter lifetimes, with the subject
       automatically being removed from the list upon destruction. It's a
       special case of the observer pattern, in which the event is the subject's
       destruction and the response is the removal from the list.
     */

    template<class T>
    class Subject;

    template<class T>
    class Observer
    {
        friend class Subject<T>;
    public:
        const std::forward_list<T*>& subjects() const
        {
            return m_subjects;
        }
    private:
        std::forward_list<T*> m_subjects;
    };

    template<class T>
    class Subject
    {
    public:
        virtual ~Subject()
        {
            for(auto& pair : m_observers) {
                pair.first->m_subjects.erase_after(pair.second);
            }
        }

        /**
           Contract: data has the same lifetime as or a greater lifetime than
           *this
         */
        void add_observer(T& data, Observer<T>& o)
        {
            o.m_subjects.push_front(&data);
            m_observers.push_front(std::make_pair(
                &o, o.m_subjects.before_begin()
            ));
        }
    private:
        std::forward_list<std::pair<
            Observer<T>*,
            typename std::forward_list<T*>::iterator
        >> m_observers;
    };
}

#endif
